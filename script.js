// Možné tahy, které hráči a počítač mohou vybrat
const tahy = ['kámen', 'papír', 'nůžky', 'tapír', 'spock'];

// Výsledky hry pro různé kombinace tahů
const výsledky = {
    'kámen': {'nůžky': 'rozdrtí', 'tapír': 'rozdrtí'},
    'papír': {'kámen': 'pokryje', 'spock': 'vyvrátí'},
    'nůžky': {'papír': 'ustřihne', 'tapír': 'usekne'},
    'tapír': {'spock': 'otráví', 'papír': 'sní'},
    'spock': {'nůžky': 'roztříští', 'kámen': 'vypaří'}
};

// Počet výher hráče, počítače a remíz
let výhryHráče = 0;
let výhryPC = 0;
let remízy = 0;

// Počet kol hry a aktuální kolo
let kola = 5;
let aktuálníKolo = 0;

// Úroveň obtížnosti počítače ('hloupy' nebo 'chytry')
let úroveňAI = 'hloupy';

// Pole uchovávající tahy hráče a počítače
let tahyHráče = [];
let tahyPC = [];

// Spuštění hry po kliknutí na tlačítko "Start hra"
document.getElementById('start-hra').addEventListener('click', startHra);

// Přidání posluchačů pro tlačítka hráče
document.querySelectorAll('.hráč-btn').forEach(button => {
    button.addEventListener('click', tahHráče);
});

// Funkce pro spuštění hry
function startHra() {
    // Inicializace proměnných
    výhryHráče = 0;
    výhryPC = 0;
    remízy = 0;
    aktuálníKolo = 0;
    kola = parseInt(document.getElementById('kola').value);
    úroveňAI = document.getElementById('uroven-ki').value;
    tahyHráče = [];
    tahyPC = [];
    updateStatus('Hra začala! Vyberte svůj tah.');
    updateStatistiky();
    document.getElementById('vítěz').innerText = '';  // Vyčistit předchozího vítěze
}

// Funkce pro zpracování tahu hráče
function tahHráče(event) {
    // Kontrola, zda hra neskončila
    if (aktuálníKolo >= kola) {
        updateStatus('Hra skončila! Obnovte stránku pro novou hru.');
        return;
    }
    
    // Získání tahu hráče
    const tahHráče = event.target.getAttribute('data-choice');
    tahyHráče.push(tahHráče);

    // Získání tahu počítače
    const tahPC = getTahPC();
    tahyPC.push(tahPC);

    // Určení vítěze kola
    const výsledek = určitVítěze(tahHráče, tahPC);
    updateStatistiky();
    aktuálníKolo++;

    // Zpráva o vítězi hry po skončení
    if (aktuálníKolo >= kola) {
        const vítěz = určitVítězeHry();
        updateStatus(`Konec hry! Hráč: ${výhryHráče}, PC: ${výhryPC}, Remízy: ${remízy}`);
        document.getElementById('vítěz').innerText = vítěz;
    }
}

// Funkce pro získání tahu počítače
function getTahPC() {
    switch(úroveňAI) {
        case 'hloupy':
            return tahy[Math.floor(Math.random() * tahy.length)];
        case 'chytry':
            return chytrýVýběrKI();
    }
}

// Funkce pro chytrý výběr tahu počítače
function chytrýVýběrKI() {
    // Pokud hráč zatím nezvolil dva tahy, počítač vybere náhodně
    if (tahyHráče.length < 2) {
        return tahy[Math.floor(Math.random() * tahy.length)];
    }

    // Pokud hráč zvolil alespoň dva tahy, počítač vybere tah na základě posledních dvou tahů hráče
    const posledníDvaTahy = tahyHráče.slice(-2);
    const možnéTahy = tahy.filter(tah => !posledníDvaTahy.includes(tah));
    return možnéTahy[Math.floor(Math.random() * možnéTahy.length)];
}

// Funkce pro určení vítěze jednoho kola
function určitVítěze(tahHráče, tahPC) {
    if (tahHráče === tahPC) {
        remízy++;
        updateStatus(`Remíza! Oba zvolili ${tahHráče}.`);
        return 'remíza';
    }

    if (výsledky[tahHráče] && výsledky[tahHráče][tahPC]) {
        výhryHráče++;
        updateStatus(`Vyhrál jste! ${tahHráče} ${výsledky[tahHráče][tahPC]} ${tahPC}.`);
        return 'hráč';
    } else {
        výhryPC++;
        updateStatus(`Prohrál jste! ${tahPC} ${výsledky[tahPC][tahHráče]} ${tahHráče}.`);
        return 'PC';
    }
}

// Funkce pro určení vítěze celé hry
function určitVítězeHry() {
    if (výhryHráče > výhryPC) {
    return 'Gratulujeme, vyhrál jste hru!';
    } else if (výhryPC > výhryHráče) {
    return 'Bohužel, PC vyhrál hru.';
    } else {
    return 'Hra skončila remízou!';
    }
    }
    
    // Aktualizace stavové zprávy na stránce
    function updateStatus(message) {
    document.getElementById('status-text').innerText = message;
    }
    
    // Aktualizace statistik na stránce
    function updateStatistiky() {
        document.getElementById('výhry-hráče').innerText = `Výhry hráče: ${výhryHráče}`;
        document.getElementById('výhry-pc').innerText = `Výhry PC: ${výhryPC}`;
        document.getElementById('remízy').innerText = `Remízy: ${remízy}`;
    }
